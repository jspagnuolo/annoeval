
### works but is inconsistent... manually it produces the correct output, in snakemake workflow, some output is not generated.
rule immcantation:
    input:
        TCR_fq = "data/{chain}/{id}_{chain}_{TCRid}.fq"
    output:
        immcant_res = "results/immcant/{chain}/{id}_{chain}_{TCRid}_germ-pass.tsv"
    params:
        in_dir = "data/{chain}",
        fq_in = "{id}_{chain}_{TCRid}.fq",
        out_dir = "results/immcant/{chain}",
        chain = "{chain}"
    threads: workflow.cores * 0.1
    message: "Running Immcantation Identification on {params.fq_in} using {threads} threads"
    shell:
        """
        docker run --rm -v /$(pwd):/tmp -w /tmp immcantation/suite:4.3.0 ./workflow/scripts/immcantation.sh {params.in_dir} {params.fq_in} {params.chain} {params.out_dir} {threads}
        """

rule trust4:
    input:
        TCR_fq = "data/{chain}/{id}_{chain}_{TCRid}.fq"
    output:
        trust_res = "results/trust4/{chain}/{id}_{chain}_{TCRid}_airr.tsv"
    params:
        prefix = "{id}_{chain}_{TCRid}",
        out_dir = "results/trust4/{chain}",
        chain = "{chain}"
    threads: 3
    message: "Running TRUST4 Identification on {params.prefix} using {threads} threads"
    shell:
        """
        docker run --rm -v /$(pwd):/tmp -w /tmp trust4 ../trust4/run-trust4 -t {threads} -f ../trust4/hg38_bcrtcr.fa --ref ../trust4/human_IMGT+C.fa -u {input.TCR_fq} -o {params.out_dir}/{params.prefix}
        """

### migmap is in a shell script because it needs a logic check to switch between TRA and TRB depending on chain
rule migmap:
    input:
        TCR_fq = "data/{chain}/{id}_{chain}_{TCRid}.fq"
    output:
        migmap_res = "results/migmap/{chain}/{id}_{chain}_{TCRid}.out"
    params:
        prefix = "{id}_{chain}_{TCRid}",
        out_dir = "results/trust4/{chain}",
        chain = "{chain}"
    threads: 3
    message: "Running MiGMAP Identification on {params.prefix} using {threads} threads"
    shell:
        """
        docker run --rm  -v /$(pwd):/tmp -w /tmp migmap ./workflow/scripts/migmap.sh {threads} {params.chain} {input.TCR_fq} {output.migmap_res}
        """

rule mitcr:
    input:
        TCR_fq = "data/{chain}/{id}_{chain}_{TCRid}.fq"
    output:
        mitcr_res = "results/mitcr/{chain}/{id}_{chain}_{TCRid}.out"
    params:
        prefix = "{id}_{chain}_{TCRid}",
        out_dir = "results/mitcr/{chain}",
        chain = "{chain}"
    threads: 3
    message: "Running MiTCR Identification on {params.prefix} using {threads} threads"
    shell:
        """
        docker run --rm  -v /$(pwd):/tmp -w /tmp mitcr ./workflow/scripts/mitcr.sh {threads} {params.chain} {input.TCR_fq} {output.mitcr_res}
        """

rule mixcr:
    input:
        TCR_fq = "data/{chain}/{id}_{chain}_{TCRid}.fq"
    output:
        mixcr_res = "results/mixcr/{chain}/{id}_{chain}_{TCRid}.txt"
    params:
        in_dir = "data/{chain}",
        fq_in = "{id}_{chain}_{TCRid}.fq",
        out_dir = "results/mixcr/{chain}",
        chain = "{chain}"
    threads: 3
    message: "Running MiXCR Identification on {params.fq_in} using {threads} threads"
    shell:
        """
        docker run --rm -v /$(pwd):/tmp -w /tmp mixcr ./workflow/scripts/mixcr.sh {params.in_dir} {params.fq_in} {params.chain} {params.out_dir} {threads}
        """