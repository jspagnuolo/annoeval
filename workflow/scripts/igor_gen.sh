#!/bin/bash

threads=$1
nTCR=$2
prefix=$3
out_dir=$4

immuno-probs -threads $threads -out-name $prefix -set-wd $out_dir generate -model human-t-alpha -n-gen $nTCR

