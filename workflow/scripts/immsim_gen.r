library(Biostrings)
library(immuneSIM)

source("./scripts/extractKmers.R")

### call: Rscript extractKmers 75 0.1
### extracts 75-mers with sample rate of 10% of kmer pool for each TCR

args = commandArgs(trailingOnly=TRUE)

simTCRa <- list()
simTCRb <- list()
for(i in 1:10)
{
  simTCRa[[paste("donor",i,sep="_")]] <- immuneSIM(number_of_seqs = 1000, species = "hs", receptor = "tr",
                                                 chain = "a", max_cdr3_length = 16, vdj_noise = 0.1)
  simTCRa[[paste("donor",i,sep="_")]]$tcr_id <- paste("donor",i,rownames(simTCRa[[paste("donor",i,sep="_")]]), sep="_")
  
  simTCRb[[paste("donor",i,sep="_")]] <- immuneSIM(number_of_seqs = 1000, species = "hs", receptor = "tr",
                                                 chain = "b", max_cdr3_length = 16, vdj_noise = 0.1)
  simTCRb[[paste("donor",i,sep="_")]]$tcr_id <- paste("donor",i,rownames(simTCRb[[paste("donor",i,sep="_")]]), sep="_")
}
saveRDS(simTCRa, file="./data/tcrsim/immsim_alpha.RDS")
saveRDS(simTCRb, file="./data/tcrsim/immsim_beta.RDS")


simTCRa <- do.call(rbind, simTCRa)

y <- simTCRa$sequence
names(y) <- simTCRa$tcr_id
y <- sapply(y, simplify = F,USE.NAMES = T, FUN = function(x){extractKmer(string=x, k=args[1], id=names(x))})

for(i in 1:length(y))
{
  names(y[[i]]) <- paste(names(y)[i], names(y[[i]]), sep="")
  y[[i]] <- y[[i]][sample.int(n=length(y[[i]]), size = length(y[[i]]*args[2]))]
  
  writeXStringSet(filepath = paste("./data/tcrsim/immsim/alpha/",names(y)[i],".fq",sep=""), 
                  x = Biostrings::DNAStringSet(y[[i]], use.names = T))
}


simTCRb <- do.call(rbind, simTCRb)

y <- simTCRb$sequence
names(y) <- simTCRb$tcr_id
y <- sapply(y, simplify = F,USE.NAMES = T, FUN = function(x){extractKmer(string=x, k=args[1], id=names(x))})

for(i in 1:length(y))
{
  names(y[[i]]) <- paste(names(y)[i], names(y[[i]]), sep="")
  y[[i]] <- y[[i]][sample.int(n=length(y[[i]]), size = length(y[[i]]*args[2]))]
  
  writeXStringSet(filepath = paste("./data/tcrsim/immsim/beta/",names(y)[i],".fq",sep=""), 
                  x = Biostrings::DNAStringSet(y[[i]], use.names = T))
}

