#!/bin/bash

mkdir ./data/tcrsim
mkdir ./data/tcrsim/igor
mkdir ./data/tcrsim/igor/alpha
mkdir ./data/tcrsim/igor/beta

docker run --rm -v /$(pwd):/tmp -w /tmp immunoprobs ./scripts/igor_gen.sh
docker run --rm -v /$(pwd):/tmp -w /tmp immunesim Rscript ./scripts/igor_kmerise.R 75 0.1 20

#mkdir ./data/tcrsim/immsim
#mkdir ./data/tcrsim/immsim/alpha
#mkdir ./data/tcrsim/immsim/beta

#docker run --rm -v /$(pwd):/tmp -w /tmp immunesim Rscript ./scripts/immsim_gen.r 75 0.1
