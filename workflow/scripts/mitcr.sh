#!/bin/bash

threads=$1
chain=$2
input=$3
output=$4

if [ "$chain" == "alpha" ];
then
    java -Xmx10g -jar ../mitcr.jar \
        -t $threads \
        -pset flex -ec 1 -pcrec smd \
        -species hs -gene TRA \
        -level 3 \
        $input \
        $output
elif [ "$chain" == "beta" ];
then
    java -Xmx10g -jar ../mitcr.jar \
        -t $threads \
        -pset flex -ec 1 -pcrec smd \
        -species hs -gene TRB \
        -level 3 \
        $input \
        $output
else
  echo 'Chain does not equal beta or beta'
  exit
fi