#!/bin/bash

threads=$1
chain=$2
input=$3
output=$4

if [ "$chain" == "alpha" ];
then
    java -Xmx16G -jar ../migmap-1.0.3/migmap-1.0.3.jar \
        --data-dir ../migmap-1.0.3/data \
        --all-alleles -S human \
        -p $threads \
        -R TRA \
        $input \
        $output
elif [ "$chain" == "beta" ];
then
    java -Xmx16G -jar ../migmap-1.0.3/migmap-1.0.3.jar \
        --data-dir ../migmap-1.0.3/data \
        --all-alleles -S human \
        -p $threads \
        -R TRB \
        $input \
        $output
else
  echo 'Chain does not equal beta or beta'
  exit
fi