#!/bin/bash

inDir=$1
fastQin=$2
chain=$3
outDir=$4
threads=$5

if [ "$chain" == "alpha" ];
then
  imgt_ref='/usr/local/share/germlines/imgt/human/vdj/imgt_human_TRAV.fasta /usr/local/share/germlines/imgt/human/vdj/imgt_human_TRAJ.fasta'
elif [ "$chain" == "beta" ];
then
  imgt_ref='/usr/local/share/germlines/imgt/human/vdj/imgt_human_TRBV.fasta /usr/local/share/germlines/imgt/human/vdj/imgt_human_TRBD.fasta /usr/local/share/germlines/imgt/human/vdj/imgt_human_TRBJ.fasta'
else
  echo 'Chain does not equal TRA or TRB'
  exit
fi
tmpdir=${fastQin/.fq/_tmp}
mkdir $tmpdir

ParseHeaders.py add --fasta -s $inDir/$fastQin --outdir ./$tmpdir --outname ${fastQin/.fq/} -f BARCODE -u ${fastQin/.fq/}
AlignSets.py muscle --nproc $threads --bf BARCODE -s ./$tmpdir/${fastQin/.fq/_reheader.fasta} --outdir ./$tmpdir --outname ${fastQin/.fq/}

BuildConsensus.py --nproc $threads -s ./$tmpdir/${fastQin/.fq/_align-pass.fasta} --outdir ./$tmpdir --outname ${fastQin/.fq/} --bf BARCODE -n 1

AssignGenes.py igblast --nproc $threads -b /usr/local/share/igblast --organism human --loci tr --format blast -s ./$tmpdir/${fastQin/.fq/_reheader.fasta} --outdir ./$tmpdir --outname ${fastQin/.fq/} 
MakeDb.py igblast -r $imgt_ref -s ./$tmpdir/${fastQin/.fq/_reheader.fasta} -i ./$tmpdir/${fastQin/.fq/_igblast.fmt7} --outdir ./$tmpdir --outname ${fastQin/.fq/}

## might need to reduce --maxmiss in production/real-life data
DefineClones.py --nproc $threads --mode gene --model hh_s1f --norm mut --maxmiss 10 --dist 1 -d ./$tmpdir/${fastQin/.fq/_db-pass.tsv} --outdir ./$tmpdir --outname ${fastQin/.fq/}
## this produces multiple entries per clone and will need to be reduced/aggregated by barcode... especially if assuming single cell.
## maybe redirect this to an R clone aggregation script?
CreateGermlines.py -r $imgt_ref -g full --cloned -d ./$tmpdir/${fastQin/.fq/_clone-pass.tsv} --outdir ./$outDir --outname ${fastQin/.fq/}

rm -rf $tmpdir