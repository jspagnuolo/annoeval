#!/bin/bash

inDir=$1
fastQin=$2
chain=$3
outDir=$4
threads=$5


mkdir ./${fastQin/.fq/_tmp}
mixcr align -t $threads -s hsa -p rna-seq -OallowPartialAlignments=true $inDir/$fastQin ./${fastQin/.fq/_tmp}/${fastQin/.fq/.vdjca}
mixcr assemblePartial ./${fastQin/.fq/_tmp}/${fastQin/.fq/.vdjca} ./${fastQin/.fq/_tmp}/${fastQin/.fq/_rescued_1.vdjca}
mixcr assemblePartial ./${fastQin/.fq/_tmp}/${fastQin/.fq/_rescued_1.vdjca} ./${fastQin/.fq/_tmp}/${fastQin/.fq/_rescued_2.vdjca}
mixcr extend ./${fastQin/.fq/_tmp}/${fastQin/.fq/_rescued_2.vdjca} ./${fastQin/.fq/_tmp}/${fastQin/.fq/_rescued_2_extend.vdjca}
mixcr assemble -t $threads ./${fastQin/.fq/_tmp}/${fastQin/.fq/_rescued_2_extend.vdjca} ./${fastQin/.fq/_tmp}/${fastQin/.fq/.clns}
if [ "$chain" == "alpha" ];
then
    mixcr exportClones -c TRA -p fullImputed -vHit -dHit -jHit -nFeatureImputed VDJTranscript ./${fastQin/.fq/_tmp}/${fastQin/.fq/.clns} $outDir/${fastQin/.fq/.txt}
elif [ "$chain" == "beta" ];
then
    mixcr exportClones -c TRB -p fullImputed -vHit -dHit -jHit -nFeatureImputed VDJTranscript ./${fastQin/.fq/_tmp}/${fastQin/.fq/.clns} $outDir/${fastQin/.fq/.txt}
else
  echo 'Chain does not equal beta or beta'
  exit
fi
rm -rf ./${fastQin/.fq/_tmp}